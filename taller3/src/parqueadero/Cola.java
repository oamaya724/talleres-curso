package parqueadero;

public class Cola<T>
{
	private Nodo first, last;
	
	private class Nodo<T>
	{
		T item;
		Nodo next;
	}
	
	public boolean isEmpty()
	{
		return first == null;
	}
	
	public void enqueue(T item)
	{
		Nodo oldLast = last;
		last = new Nodo();
		last.item = item;
		last.next = null;
		if(isEmpty()) first = last;
		else oldLast.next = last;
	}
	
	public T dequeue()
	{
		T item = (T) first.item;
		first = first.next;
		if (isEmpty()) last = null;
		return item;
	}
}