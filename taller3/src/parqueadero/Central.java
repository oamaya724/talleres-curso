package parqueadero;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;


public class Central 
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	Cola<Carro> colaCarros = new Cola<Carro>();
	
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	Pila<Carro> p1 = new Pila<Carro>();
	Pila<Carro> p2 = new Pila<Carro>();
	Pila<Carro> p3 = new Pila<Carro>();
	Pila<Carro> p4 = new Pila<Carro>();
	Pila<Carro> p5 = new Pila<Carro>();
	Pila<Carro> p6 = new Pila<Carro>();
	Pila<Carro> p7 = new Pila<Carro>();
	Pila<Carro> p8 = new Pila<Carro>();
	
	/**
	 * ArrayList con las pilas de carros
	 */
	
	ArrayList parqueaderos;

    
	
    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	
	Pila<Carro> pT = new Pila<Carro>();
   
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
        //De la forma en que se crearon las pilas y las colas anteriormente, ya quedaron inicializadas.
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro nuevo = new Carro (pColor, pMatricula, pNombreConductor);
    	colaCarros.enqueue(nuevo);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	
    	parqueaderos.add(p1);
    	parqueaderos.add(p2);
    	parqueaderos.add(p3);
    	parqueaderos.add(p4);
    	parqueaderos.add(p5);
    	parqueaderos.add(p6);
    	parqueaderos.add(p7);
    	parqueaderos.add(p8);
    	
    	Carro parqueado = colaCarros.dequeue();
    	
    	for (int i = 0; i < parqueaderos.size(); i++)
    	{
    		Pila actual = (Pila)parqueaderos.get(i);
    		
    	}
      return "";
    } 
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	return 0.0;
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	return "PARQUEADERO X";    	    
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {

    	return null;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	return null;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	return null;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	return null;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	    	return null;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	
    	return null;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	return 0.0;	
    }
}
