package parqueadero;

public class Pila<T>
{
	private Nodo first = null;
	
	private class Nodo<T>
	{
		T item;
		Nodo next;
	}
	
	public boolean isEmpty()
	{
		return first == null;
	}
	
	public void push(T item)
	{
		Nodo oldFirst = first;
		first = new Nodo();
		first.item = item;
		first.next = oldFirst;
	}
	
	public T pop()
	{
		T item = (T) first.item;
		first = first.next;
		return item;
	}
}